alias ll='ls -lF'
alias l='ls -alF'

alias gits='git status'
alias gs='git status'

alias clipboard='xclip -selection clipboard'