#!/usr/bin/env bash
SCRIPT_DIR=$(dirname $0)

STOW_TARGET=$HOME
STOW_DIRECTORY=$(pwd)
STOW_COMMAND="stow -t $STOW_TARGET -d $STOW_DIRECTORY"

if [[ "$#" == "0" ]]; then
    echo "No theme specified"
    exit 1;
fi

THEME_PREFIX=i3-theme-

OLD_THEME=$THEME_PREFIX$(cat $HOME/.config/i3-theme/id)
NEW_THEME=$THEME_PREFIX$1

echo "Old theme: $OLD_THEME"
echo "New theme: $NEW_THEME"


if [ ! -d $OLD_THEME ]; then
    echo "Old theme $OLD_THEME not found!";
else
    $STOW_COMMAND -D $OLD_THEME;
fi

if [ ! -d $NEW_THEME ]; then
    echo "Theme $NEW_THEME not found! Aborting..."
    exit 1;
fi

$STOW_COMMAND $NEW_THEME || exit $?

~/.config/i3/merge-configs.sh || exit $?
i3-msg restart
