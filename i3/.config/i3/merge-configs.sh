#!/usr/bin/env bash
SCRIPT_DIR=`dirname $0`
CONFIG_OUTPUT=$SCRIPT_DIR/config
CONFIG_PATTERN=$SCRIPT_DIR/config.[0-9]

rm -f $CONFIG_OUTPUT
echo -n > $CONFIG_OUTPUT

for f in $(ls $CONFIG_PATTERN); do
    cat $f >> $CONFIG_OUTPUT;
done
